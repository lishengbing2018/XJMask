//
//  ViewController.swift
//  XJMask
//
//  Created by lishengbing on 10/30/2017.
//  Copyright (c) 2017 lishengbing. All rights reserved.
//

import UIKit
import XJMask

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        /*
        sl_presentAlertViewController(style: SLMaskStyle(), { (leftBtn) in
         
        }) { (rightBtn) in
         
        }*/
        
        let v = TestView(frame: CGRect.zero)
        v.backgroundColor = UIColor.orange
        let style = SLMaskStyle()
        style.presentAnimDirection = .moveDown
        style.presentedFrame = CGRect(x: 0, y: UIScreen.main.bounds.height - 300, width: UIScreen.main.bounds.width, height: 300)
        
        let vc = SLMaskVC(style: style, childView: v)
        present(vc, animated: true, completion: nil)
        
        
        v.btnCallBack = { [weak self] (btn) in
            print("点击确定了...")
            
            let v2 = UIView()
            v2.backgroundColor = UIColor.black
            let style = SLMaskStyle()
            style.presentAnimDirection = .moveDown
            style.presentBackgroundColor = UIColor.clear
            style.presentAlpha = 0
            style.presentedFrame = CGRect(x: 20, y: UIScreen.main.bounds.height - 220, width: UIScreen.main.bounds.width - 40, height: 200)
            let vc2 = SLMaskVC(style: style, childView: v2)
            vc.present(vc2, animated: true, completion: nil)
        }
    }

}

extension ViewController: SLMaskAlertVCable {
    
}

