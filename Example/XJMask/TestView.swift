//
//  TestView.swift
//  XJMask_Example
//
//  Created by shanlin on 2017/11/9.
//  Copyright © 2017年 CocoaPods. All rights reserved.
//

import UIKit

class TestView: UIView {

    fileprivate lazy var btn: UIButton = UIButton()
    typealias TestViewBtnCallBack = (_ btn: UIButton) -> ()
    var btnCallBack: TestViewBtnCallBack?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        btn.frame = CGRect(x: 0, y: 20, width: btn.bounds.width, height: btn.bounds.height)
        btn.center.x = self.center.x
    }

}

extension TestView {
    fileprivate func setupUI() {
        addSubview(btn)
        btn.setTitle("确定", for: .normal)
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.addTarget(self, action: #selector(btnClick), for: .touchUpInside)
        btn.sizeToFit()
    }
    
    @objc func btnClick() {
        if btnCallBack != nil {
            btnCallBack!(btn)
        }
    }
}
