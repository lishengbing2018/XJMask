//
//  SLMaskAlertVCable.swift
//  蒙版层组件
//
//  Created by 李胜兵 on 2017/10/22.
//  Copyright © 2017年 shanlin. All rights reserved.
//


import UIKit


//public enum SLAlertControllerStyle : Int {
//    case actionSheetStyle
//    case alertStyle
//}

public protocol SLMaskAlertVCable {
    
}

extension SLMaskAlertVCable where Self: UIViewController {
   public func sl_presentAlertViewController(style: SLMaskStyle, _ leftDoneCallback: @escaping(_ handler: UIButton) -> (), rightDoneCallBack: @escaping(_ handler: UIButton) -> ()) {
        let p = SLMaskAlertVC(style: style)
        p.btnClick(leftClickCallBack: leftDoneCallback, rightClickCallBack: rightDoneCallBack)
        self.present(p, animated: true, completion: nil)
    }
}

extension SLMaskAlertVCable where Self: UIView {
   public func sl_presentAlertViewController(style: SLMaskStyle, _ leftDoneCallback: @escaping(_ handler: UIButton) -> (), rightDoneCallBack: @escaping(_ handler: UIButton) -> ()) {
        let p = SLMaskAlertVC(style: style)
        p.btnClick(leftClickCallBack: leftDoneCallback, rightClickCallBack: rightDoneCallBack)
        Self.sl_viewController(inView: self)?.present(p, animated: true, completion: nil)
    }
    
   public static func sl_viewController(inView: UIView) -> UIViewController? {
        var nextVC = inView.next
        while(nextVC != nil) {
            if (nextVC?.isKind(of: UIViewController.self))! {
                return nextVC as? UIViewController
            }
            nextVC = nextVC?.next
        }
        return nil;
    }
}

extension SLMaskAlertVCable where Self: NSObject {
   public func sl_presentAlertViewController(presentVC: UIViewController, style: SLMaskStyle, _ leftDoneCallback: @escaping(_ handler: UIButton) -> (), rightDoneCallBack: @escaping(_ handler: UIButton) -> ()) {
        let p = SLMaskAlertVC(style: style)
        p.btnClick(leftClickCallBack: leftDoneCallback, rightClickCallBack: rightDoneCallBack)
        assert(UIApplication.shared.keyWindow?.rootViewController != nil, "请检查一下你的根控制器是不是启动的时候没有设置！")
        presentVC.present(p, animated: true, completion: nil)
    }
}

