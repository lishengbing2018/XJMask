//
//  SLMaskVC.swift
//  蒙版层组件
//
//  Created by shanlin on 2017/10/20.
//  Copyright © 2017年 shanlin. All rights reserved.
//

import UIKit

public class SLMaskVC: UIViewController {

    fileprivate var style: SLMaskStyle!
    fileprivate var childView: UIView!
    public typealias SLMaskVCPresentedStatus = (_ isPresented: Bool) -> ()
    public var isPresentedCallback: SLMaskVCPresentedStatus?
    
    fileprivate lazy var popoverAnim: SLPopoverAnimator = SLPopoverAnimator { [weak self] (isPresented) in
        if self?.isPresentedCallback != nil {
            self?.isPresentedCallback!(isPresented)
        }
    }
    
    public init(style: SLMaskStyle, childView: UIView) {
        super.init(nibName: nil, bundle: nil)
        self.style = style
        self.childView = childView
        setupUI()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        //print("deinit----\(self.classForCoder)")
    }
    
    override public func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        childView.frame = CGRect(x: 0, y: 0, width: style.presentedFrame.width, height: style.presentedFrame.height)
    }
}

extension SLMaskVC: SLPopoverAnimatorable {
    fileprivate func setupUI() {
        willBecomeTransitioningDelegate(popoverAnim: popoverAnim, style: style)
        view.addSubview(childView)
    }
}

